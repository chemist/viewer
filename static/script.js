$(document).ready(function () {
  function loadTags(project) {
      $.ajax({
          url: "/tags?project=" + project
      }).done(function (e) {
          $('span.n, span.kt, span.nf, span.nn', $('pre')).filter(function(){ return $(this).text() in e; }).addClass('clickable').on('click', function (e) {
              var fun = $(e.target).text();
              console.log("fun", fun);
              $.ajax({
                  url: "/tag?project=" + project + "&tag=" + fun + "&current=" + window.location.pathname,
                  statusCode: {
                      200: goto
                  }
              })
 
          })
      });
  }
  $(document).pjax('a', '#pjax-container', { timeout: 10000 })
  $(document).on('pjax:end', function () {
      var line = window.location.hash.split('#')[1]
      if (line) {
         var first = $("[name='L-1']")
         var need = $('[name="' + line + '"]')
         var newline = need.offset().top - first.offset().top + first.scrollTop();
         $(window).scrollTop(newline);
      }
      var project = window.location.pathname.split('/')[2];
      loadTags(project);
      breadcrumbs();
  });
 
  var project = window.location.pathname.split('/')[2];
  loadTags(project);
 
 
  function goto(e) {
      console.log("goto ", e);
      $.pjax({url: e, container: '#pjax-container'});
  };
 
  function breadcrumbs () {
      var list = window.location.pathname.split('#')[0].split('/')
          current = list.map(function(e,i) {
              if (i == 0) {
                  return "<li> </li>"
              } else {
                  return "<li> <a href='" + list.slice(0,i + 1).join('/') + "'>" + e + "</a></li>"
              };
          });
      $('#breadcrumb').empty();
      $('#breadcrumb').append(current.join(''))
     
  };
  breadcrumbs();
 
});
