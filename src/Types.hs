{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Types where
import           Data.Text.Lazy                       (Text)
import qualified Data.Text  as T
import           Data.Map.Strict                      (Map)
import           Data.Vector                          (Vector)
import Data.Aeson
import Control.Applicative
import Control.Monad (mzero)
import Data.Hashable

newtype ProjectName = ProjectName { unPr :: Text } deriving (Eq, Ord, Show)

data Jtree = Jtree 
  { nodeId :: Text
  , nodeText :: Text
  , nodeChildren :: [Jtree]
  } deriving Show

instance ToJSON Jtree where
    toJSON x = object [ "id" .= nodeId x, "text" .= nodeText x, "children" .= toJSON (nodeChildren x) ] 

instance ToJSON ProjectName where
    toJSON (ProjectName x) = object ["project" .= x, "id" .= x, "text" .= x]

instance FromJSON ProjectName where
    parseJSON (Object v) = ProjectName <$> v .: "project"
    parseJSON _ = mzero


type Project = Vector Tag
type Projects = Map ProjectName Project


newtype TagName = TagName T.Text deriving (Show, Eq, Ord, Hashable)
newtype TagFile = TagFile { unF :: T.Text } deriving (Show, Eq, Ord)
newtype TagAddress = TagAddress { unA :: T.Text } deriving (Show, Eq, Ord)
newtype TagComment = TagComment T.Text deriving (Show, Eq, Ord)

data Tag = Tag 
  { tagName    :: TagName
  , tagFile    :: TagFile 
  , tagAddress :: TagAddress 
  , tagComment :: TagComment
  } deriving (Show)


