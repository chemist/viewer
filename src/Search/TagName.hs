{-# OPTIONS_GHC -fno-warn-orphans #-}
module Search.TagName (find, find', onlyFT) where

import Types 
import Data.Vector (Vector, (!), slice, length, filter, map, toList)
import Data.HashMap.Strict (fromList, HashMap)
import Data.Text (Text)
import Data.Aeson
import System.FilePath
import qualified Data.ByteString as BS
import Text.Highlighter (runLexer, lexerFromFilename, tText, tType)
import Control.Applicative ((<$>))
import Prelude hiding (length, fst, snd, filter, map)
import qualified Prelude
import Debug.Trace

instance Eq Tag where
    Tag a _ _ _ == Tag b _ _ _ = a == b

instance Ord Tag where
    compare (Tag a _ _ _) (Tag b _ _ _) = compare a b

binarySearch :: Vector Tag -> TagName -> Int -> Int -> Maybe (Vector Tag)
binarySearch v tn lo hi 
    | hi < lo = Nothing
    | tagName pivot > tn = binarySearch v tn lo (mid -1)
    | tagName pivot < tn = binarySearch v tn (mid + 1) hi
    | otherwise = Just  $ getSlice (goLeft mid) (goRight mid)
  where
    mid = lo + (hi - lo) `div` 2
    pivot = v ! mid
    getSlice s e = slice s (e - s + 1) v
    goLeft x  | x <= 0 = x
              | tagName (v ! pred x) == tn = goLeft $ pred x
              | otherwise = x
    goRight x | x >= length v = x
              | tagName (v ! succ x) == tn = goRight $ succ x
              | otherwise = x

find :: String -> Vector Tag -> TagName -> Maybe (Vector Tag)
find current v t = trace (show current) binarySearch v t 0 (length v - 1)

find' :: String -> TagName -> Vector Tag -> Maybe (Vector Tag)
find' current t v = trace (show current) binarySearch v t 0 (length v - 1)

data TypeFile = Haskell
              | Python
              deriving (Eq, Show)

typeFile :: String -> TypeFile
typeFile fp = case splitExtension fp of
                   (_, "hs") -> Haskell
                   (_, "py") -> Python
                   _ -> undefined

getImports :: String -> IO (Vector TagFile)
getImports fp 
  | typeFile fp == Haskell = getHaskellImports fp
  | typeFile fp == Python = getPythonImports fp
  | otherwise = error "uncown file type"

getHaskellImports, getPythonImports :: String -> IO (Vector TagFile)
getPythonImports = undefined

getHaskellImports = undefined

getHaskellImports' fp = do
    f <- BS.readFile fp
    case lexerFromFilename fp of
         Nothing -> undefined
         Just l -> return $ Prelude.map tText <$> Prelude.filter (\x -> show (tType x) == "Arbitrary \"Name\" :. Arbitrary \"Namespace\"") <$> runLexer l f


un :: Tag -> Text
un (Tag (TagName x) _ _ _) = x

onlyFT :: Vector Tag -> HashMap Text Value
onlyFT vt = fromList $ zip (toList $ map un vt) (repeat Null)

    


