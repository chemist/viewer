{-# LANGUAGE OverloadedStrings #-}
module Page where

import           Control.Monad 
import           Data.Monoid
import           Text.Blaze.Html5              hiding (style, map)

import qualified Text.Blaze.Html5              as H
import           Text.Blaze.Html5.Attributes
import qualified Text.Blaze.Html5.Attributes   as A

import           Data.Text.Lazy                (Text)
-- import Debug.Trace (trace)

import           Prelude                       hiding (div, head, id, span)

index :: Html -> Html
index body' = do
    head $ do
        docType
        meta ! charset "utf-8"
        meta ! name "viewport" ! content "width=device-width, initial-scale=1"
        meta ! name "haskell.su"
        meta ! name "Smirnov Alexey"
        script ! src "/jquery.min.js" $ z
        script ! src "/jquery.pjax.js" $ z
        link ! rel "stylesheet" ! href "/normalize.css"
        link ! rel "stylesheet" ! href "/bootstrap.min.css"
        link ! rel "stylesheet" ! href "/bootstrap-theme.min.css"
        link ! rel "stylesheet" ! href "/dashboard.css"
        script ! src "/bootstrap.min.js" $ z
        script ! src "/script.js" $ z
        link ! rel "stylesheet" ! href "/monokai.css"
        H.style "body { padding-top: 50px; } " 
        H.style ! id "holderjs-style" ! type_ "text/css" $ z
    body $ do
        div ! class_ "navbar navbar-default navbar-fixed-top" ! role "navigation" $ do
            div ! class_ "container-fluid" $ do
                div ! class_ "navbar-header" $ do
                    a ! class_ "navbar-brand" ! href "/" $ "haskell.su"
                div ! class_ "collapse navbar-collapse" $ 
                    ul ! class_ "nav navbar-nav" $ 
                        li $ 
                            ol ! class_ "breadcrumb" ! id "breadcrumb" $
                                li $ a  ! href "/projects" $ "projects"
                            --    li $ a  ! href "/projects/fixmon" $ "fixmon"
        div ! class_ "container-fluid" $
           div ! class_ "row" $ do
               div ! class_ "col-sm-3 col-md-2 sidebar" $
                   div ! id "jstree_files" $ mempty
               div ! id "pjax-container" ! class_ "col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" $ body' -- >> yandex

yandex :: Html
yandex = do
    a ! href "https://metrika.yandex.ru/stat/?id=25805831&amp;from=informer" ! target "_blank" ! rel "nofollow" $
        img ! src "//bs.yandex.ru/informer/25805831/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" ! style "width:88px; height:31px; border:0;" ! alt "Яндекс.Метрика" 
            ! A.title  "Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" ! onclick "try{Ya.Metrika.informer({i:this,id:25805831,lang:'ru'});return false}catch(e){}"
    script ! type_ "text/javascript" $ " (function (d, w, c) {(w[c] = w[c] || []).push(function() { try {w.yaCounter25805831 = new Ya.Metrika({id:25805831,webvisor:true,clickmap:true,trackLinks:true,accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName('script')[0], s = d.createElement('script'), f = function () { n.parentNode.insertBefore(s, n); }; s.type = 'text/javascript'; s.async = true; s.src = (d.location.protocol == 'https:' ? 'https:' : 'http:') + '//mc.yandex.ru/metrika/watch.js'; if (w.opera == '[object Opera]') { d.addEventListener('DOMContentLoaded', f, false); } else { f(); } })(document, window, 'yandex_metrika_callbacks'); "
    noscript $ div $ img ! src "//mc.yandex.ru/watch/25805831" ! style "position:absolute; left:-9999px;" ! alt ""

---------------------------------------------------------------------------------------------------------------------------------------

paginator :: Maybe Int -> Int -> Text -> Html -> Html
paginator Nothing n l parent = paginator (Just 1) n l parent
paginator (Just current) n l parent
  | n > 1 = parent >> do
    ul ! class_ "pagination" $ do
        li $ a ! href (toValue l <> "?page=" <> toValue (fasterMin current)) $ "<<"
        forM_ (range current n) $ \x -> li ! active current x $ a ! href (toValue l <> "?page=" <> toValue x)  $ toHtml x
        li $ a ! href (toValue l <> "?page=" <> toValue (fasterMax current n)) $ ">>"
  | otherwise = parent
  where
    range :: Int -> Int -> [Int]
    range x size' 
      | x - 5 < 1 && x + 10 <= size' = [1 .. 9]
      | x - 5 < 1 && x + 10 > size' = [1 ..  size']
      | x + 4 > size' = [size' - 8 .. size']
      | otherwise = [x - 4 ..  x + 4]
    fasterMin :: Int -> Int
    fasterMin x 
      | x - 9 <= 1 = 1
      | otherwise = x - 9
    fasterMax :: Int -> Int -> Int
    fasterMax x size'
      | x + 9 >= size' = size'
      | otherwise = x + 9
    active :: Int -> Int -> Attribute
    active x y | x == y = class_ "active"
               | otherwise = mempty

z :: Html
z = mempty

role :: AttributeValue -> Attribute
role = customAttribute "role"

dataToggle :: AttributeValue -> Attribute
dataToggle = dataAttribute "toggle"

container :: Html -> Html
container x = H.div ! class_ "container" $ x

dataTarget :: AttributeValue -> Attribute
dataTarget = dataAttribute "target"

toA :: Text -> AttributeValue
toA = toValue
---------------------------------------------------------------------------------------------------------------------------------------
