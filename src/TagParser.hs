{-# LANGUAGE OverloadedStrings #-}
module TagParser 
( parserTags
) where

import Data.Attoparsec.Text
import Data.Vector hiding (takeWhile, dropWhile)
import Data.Text hiding (takeWhile)
import Control.Applicative
import Control.Monad (void)
import Data.Either (rights)
import qualified Data.Text.IO as IT
import Types
import Prelude hiding (takeWhile, dropWhile)

normalize :: TagFile -> TagFile
normalize (TagFile xs) = TagFile $ dropWhile (\x -> x == '.' || x == '/') xs

parserTags :: String -> IO (Vector Tag) 
parserTags fp = do
    f <- IT.readFile fp
    case parseOnly tagsParser f of
         Left e -> fail e
         Right t -> return $ fromList $ rights t

tagsParser :: Parser [Either () Tag] 
tagsParser = (tagParser `sepBy` endOfLine) 

tagParser :: Parser (Either () Tag)
tagParser = eitherP ctagsCommentP (simple <|> lastP)
  where
  simple = Tag <$> tagNameP <* char '\t'  <*> tagFileP <* char '\t' <*> tagAddressP  <*> tagCommentP 

lastP :: Parser Tag
lastP = do
    n <- tagNameP
    void $ char '\t'
    f <- normalize <$> tagFileP
    void $ char '\t'
    a <- TagAddress <$> (stripC <$> takeTill (== '\t') <|> takeText)
    c <- TagComment <$> takeText
    return $ Tag n f a c
    where stripC = dropWhileEnd (\x -> x == ';' || x == '"')


tagNameP :: Parser TagName
tagNameP = TagName <$> takeTill (== '\t')

tagFileP :: Parser TagFile
tagFileP = normalize . TagFile <$> takeTill (== '\t')

tagAddressP :: Parser TagAddress
tagAddressP = TagAddress <$> (stripC <$> takeTill (inClass "\n\t") )
  where stripC = dropWhileEnd (\x -> x == ';' || x == '"')

tagCommentP :: Parser TagComment
tagCommentP = TagComment <$> (dropWhile (== '\t') <$> (takeTill isEndOfLine <|> pure "" ))

ctagsCommentP :: Parser ()
ctagsCommentP = char '!' *> takeTill isEndOfLine *> pure ()
