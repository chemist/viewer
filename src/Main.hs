{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import           Network.HTTP.Types                   (status204)
import           Network.Wai.Middleware.RequestLogger
import           Network.Wai.Middleware.Static        hiding ((<|>))
import           Web.Scotty                           hiding (File)
import qualified Web.Scotty                           as W

import           Text.Blaze.Html.Renderer.Text        (renderHtml)

import qualified Data.ByteString                      as BS
import           Data.Text.Lazy                       (Text)
import qualified Data.Text.Lazy                       as T
import qualified Data.Text.Lazy.IO                    as T
import           Text.Highlighter                     (lexerFromFilename,
                                                       runLexer)
import           Text.Highlighter.Formatters.Html     (format)

import           System.Directory
import           System.FilePath

import           Control.Applicative
import           Control.Concurrent.MVar
import           Control.Exception                    (SomeException, try)
import           Control.Monad
import           Control.Monad.IO.Class               (liftIO)
import           Data.Maybe
import           Data.Monoid

import           Text.Blaze.Html5                     (Html, a, li, pre, span,
                                                       toHtml, toValue, ul, (!))
import           Text.Blaze.Html5.Attributes          (class_, href)
import           Text.Blaze.Internal                  (Attribute)

import qualified Data.Map.Strict                      as M
import           Data.Vector                          (Vector, head)

import           Debug.Trace                          (trace)
import           Prelude                              hiding (head, lines,
                                                       putStr, span, takeWhile,
                                                       words)
--
import           Dos2Unix
import           Page
import           Search.TagName
import           TagParser
import           Types

main :: IO ()
main = do
    tags <- newMVar M.empty
    scotty 3000 $ do

      middleware $ staticPolicy (addBase "static" >-> noDots)

      middleware logStdoutDev

      get (capture "/") $ do
          isContainer' <-  isContainer <$> W.params
          page <- pageNumber <$> W.params
          srcView page (addHead isContainer') "/projects"

      get (capture "/projects") $ do
          isContainer' <-  isContainer <$> W.params
          page <- pageNumber <$> W.params
          srcView page (addHead isContainer') "/projects"

      get (regex "^/projects/([^/]+)/?[^?#]*$") $ do
          path <- W.param "0"
          project <- ProjectName <$> W.param "1"
          isContainer' <-  isContainer <$> W.params
          page <- pageNumber <$> W.params
          loadTagsAction tags project
          srcView page (addHead isContainer') $ stripLastSlash path

      get (capture "/tag") $ do
          project <- ProjectName <$> W.param "project"
          tag <- TagName <$> W.param "tag"
          current <- W.param "current"
          loadTagsAction tags project
          result <- liftIO $ withMVar tags (\x -> return (join $ find' current tag <$> M.lookup project x))
          case result of
               Just resp -> W.text $ linkToUrl resp project
               Nothing -> noContent
      get (capture "/tags") $ do
          project <- ProjectName <$> W.param "project"
          loadTagsAction tags project
          result <- liftIO $ withMVar tags (\x -> return (M.lookup project x))
          W.json $ onlyFT <$> result

      get (regex ".*$") $ redirect "/"

isContainer :: [Param] -> Bool
isContainer x = case lookup "_pjax" x of
                     Nothing -> False
                     Just _ -> True

pageNumber :: [Param] -> Maybe Int
pageNumber x = case lookup "page" x of
                    Nothing -> Nothing
                    Just y -> toI $ reads (T.unpack y)
               where
               toI [(i,_)] = Just i
               toI _ = Nothing

linkToUrl :: Vector Tag -> ProjectName -> Text
linkToUrl x (ProjectName n) = "/projects/" <> n <> "/" <> (T.fromStrict . unF . tagFile . head $ x) <> "#L-" <> (T.fromStrict . unA . tagAddress . head $ x)

loadTagsAction :: MVar Projects -> ProjectName -> ActionM ()
loadTagsAction tags project = do
          m <- liftIO $ withMVar tags (isProjectAvailable project)
          unless m $ do
              liftIO $ print $ "parse and load tags for " <> show  project
              let tagsPath = "./projects/" <> unPr project <> "/tags"
              fe <- liftIO $ doesFileExist $ T.unpack tagsPath
              unless fe next
              tm <-  liftIO . try . parserTags  $ T.unpack tagsPath
              either bad (loadTags project tags) tm

noContent :: ActionM ()
noContent = status status204

----------------------------------------------------------------------------------------------------------
--  function for work with projects
----------------------------------------------------------------------------------------------------------
isProjectAvailable :: ProjectName -> Projects -> IO Bool
isProjectAvailable x = return . M.member x

stripLastSlash :: Text -> Text
stripLastSlash = T.dropWhileEnd (== '/')

data File = File
  { filePath :: String
  , fileType :: FileType
  } deriving Show

data FileType = RegularFile | Directory | NotUsableType deriving Show

getFileType :: String -> IO FileType
getFileType path = do
    isF <- doesFileExist path
    isD <- doesDirectoryExist path
    case (isF, isD) of
         (True, False) -> return RegularFile
         (False, True) -> return Directory
         _ -> return NotUsableType

pathToFile :: String -> IO File
pathToFile x = File <$> return (drop 1 x) <*> getFileType x


lsFiles :: String -> IO [File]
lsFiles x = do
    filtered <- filter testFiles <$> getDirectoryContents x
    mapM (\y -> pathToFile (x <> "/" <> y)) filtered
  where
  testFiles ('.':_) = False
  testFiles _ = True

----------------------------------------------------------------------------------------------------------
--  function for load and parse tags file
----------------------------------------------------------------------------------------------------------
loadTags :: ProjectName -> MVar Projects -> Vector Tag -> ActionM ()
loadTags project m l = do
    liftIO $ modifyMVar_ m (return . M.insert project l)
    return ()

bad :: SomeException -> ActionM ()
bad _ = fail "cant work with this project, bad function"

srcView :: Maybe Int -> (Html -> Html) -> Text  -> ActionM ()
srcView page addHead' project = do
    let fullPatch = "." <> project
        notPoint = not $ T.isInfixOf ".." project
    whichType <- liftIO $ getFileType $ T.unpack fullPatch
    case (notPoint, whichType) of
         (True, Directory) -> do
             ls <- liftIO $ lsFiles (T.unpack fullPatch)
             W.html . renderHtml $ addHead' $ paginator page (1 + length ls `div` paginatorSize) project $ showDirectory $ onlyPage page ls
         (True, RegularFile) -> do
           (wr :: Either SomeException Html) <- liftIO $ try $ paintHtml (T.unpack fullPatch)
           either (const (showRawFile addHead' (T.unpack fullPatch))) (W.html . renderHtml . addHead') wr
         _ -> trace (show notPoint <> show whichType ) W.html "not found"

addHead :: Bool -> Html -> Html
addHead False x = index x
addHead True x = x

showRawFile :: (Html -> Html) -> FilePath -> ActionM ()
showRawFile addHead' path = do
    f <- liftIO $ T.readFile path
    W.html . renderHtml . addHead' . (pre ! class_ "raw") . toHtml $ f

paginatorSize :: Int
paginatorSize = 20

onlyPage :: Maybe Int -> [File] -> [File]
onlyPage Nothing = onlyPage (Just 1)
onlyPage (Just x) = take paginatorSize . snd . splitAt ((x - 1) * paginatorSize)

showDirectory :: [File] -> Html
showDirectory ls = ul ! class_ "list-unstyled" $ forM_ ls fileLi

fileLi :: File -> Html
fileLi x = li $ fileToLink x

fileToLink :: File -> Html
fileToLink x = let fp = filePath x
               in span ! fileToClass x $ do
                   void " "
                   a ! href (toValue fp) $ toHtml . snd . splitFileName $ fp

fileToClass :: File -> Attribute
fileToClass x = case fileType x of
                     RegularFile -> class_ "glyphicon glyphicon-file"
                     Directory -> class_ "glyphicon glyphicon-folder-close"
                     _ -> class_ "bad"

paintHtml :: String -> IO Html
paintHtml fn = maybe baddy good $ lexerFromFilename fn
    where
    baddy = fail "unknown file type"
    good l = do
        s <- d2u <$> BS.readFile fn
        return $ either (fail . show) (format True) (runLexer l s)

