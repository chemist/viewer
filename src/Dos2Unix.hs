module Dos2Unix where

import Data.ByteString.Char8 hiding (lines)
import Prelude hiding (lines, unlines, readFile, drop, take, null)

d2u :: ByteString -> ByteString
d2u = unlines . lines

lines :: ByteString -> [ByteString]
lines ps
    | null ps = []
    | otherwise = case search ps of
                     Nothing -> [ps]
                     Just n  -> take n ps : lines (drop (n+remove) ps)
    where remove = maybe 1 (const 2) $ elemIndex '\r' ps
          search = case elemIndex '\r' ps of 
                      Just _ ->  elemIndex '\r'
                      Nothing -> elemIndex '\n'
